# pre-commit-debian - pre-commit hooks for debian packaging
#
# Copyright (C) 2022  Entr'ouvert
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

import argparse
import subprocess


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('files', nargs=argparse.REMAINDER)
    args = parser.parse_args()

    cmd = ['wrap-and-sort', '--wrap-always', '--keep-first', '--trailing-comma']
    for file in args.files:
        cmd.append('-f')
        cmd.append(file)
    subprocess.run(cmd, check=True)
